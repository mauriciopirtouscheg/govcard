from flask import Flask
from . import index

def create_app():
    # create and configure the app
    app = Flask(__name__)
    app.register_blueprint(index.bp)
    return app
